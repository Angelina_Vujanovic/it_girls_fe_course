import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = { users : [] };
  }

  componentDidMount() {
      fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      .then(data => {
          this.setState( {users: data });
      });
  }

  render() {
    const heading=["Name", "Username","Email", "Address","Phone", "Website", "Company"];
    
    return (  
      <table>
        <thead>
            <tr>
            {
                heading.map((item, index) => 
                    <th key={index}>{item}</th>
                )
            }
            </tr>
        </thead>
        <tbody>
        {
          this.state.users.map(user => 
            <tr key={user.id}>
              <td>{user.name}</td>
              <td>{user.username}</td>
              <td>{user.email}</td>
              <td>{user.address.street},{user.address.city},{user.address.zipcode}</td>
              <td>{user.phone}</td>
              <td>{user.website}</td>
              <td>{user.company.name}</td>
            </tr>
          )
        }
        </tbody>
      </table>
    );
  }
}

export default App;