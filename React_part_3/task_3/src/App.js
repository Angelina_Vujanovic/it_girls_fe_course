import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = { albums : [], albumPhotos: [], currentAlbumId: null};
  }

  componentDidMount() {
     fetch('https://jsonplaceholder.typicode.com/albums')
     .then(response => response.json())
     .then(data => this.setState({albums: data}));
  }

  getAlbumPhotos = (albumId) => {
    const url='https://jsonplaceholder.typicode.com/albums/'+albumId+'/photos';
    this.setState({currentAlbumId: albumId});
    
    fetch(url)
    .then(response => response.json())
    .then(data => this.setState({albumPhotos: data.slice(1,6)}));
  }

  render() {
 
    return (  
      <div className="albums"> 
        <h2>List of albums</h2> 
        <ul>
          {
            this.state.albums.map(album => 
              <li key={album.id}>{album.title} <button className="detail_btn" onClick={()=>this.getAlbumPhotos(album.id)}>Details</button>
                {
                  this.state.albumPhotos.length > 0 && album.id === this.state.currentAlbumId &&
                    <table>
                        <thead>
                          <tr><th>Title</th><th>Picture</th></tr>
                        </thead>
                        <tbody>
                          {
                            this.state.albumPhotos.map(photo => 
                              <tr key={photo.id}>
                                <td>{photo.title}</td>
                                <td>
                                  <a href={photo.url} target="_blank" rel="noopener noreferrer">
                                    <img src={photo.thumbnailUrl} alt={photo.title}></img>
                                  </a>
                                </td>
                              </tr>  
                            )
                          }
                        </tbody>
                    </table>
                }
              </li>
            )
          }
        </ul>
      </div>
    );
  }
}

export default App;